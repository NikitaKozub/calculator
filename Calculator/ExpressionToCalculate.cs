﻿using System;
using System.Text;

namespace Calculator
{
    /// <summary>
    /// Текущее выражение
    /// </summary>
    public class ExpressionToCalculate
    {
        public int CountOperator;
        public int CountNumbers;
        public string[] numbers;
        public string[] operation;//знак
        public StringBuilder Input;

        public ExpressionToCalculate()
        {
            CountOperator = 0;
            CountNumbers = 0;
        }

        /// <summary>
        /// Подсчет операторов
        /// </summary>
        public void FindCountOperator()
        {
            for (int i = 1; i < Input.Length; i++)
            {
                if (Input[i] == '+' || Input[i] == '-' ||
                    Input[i] == '*' || Input[i] == '/' || 
                    Input[i] == '^')
                {
                    if (Input[i + 1] != '-')
                    {
                        CountOperator++;
                    }
                }
            }
        }
        /// <summary>
        /// Подсчет количества чисел
        /// </summary>
        public void FindCountNumbers()
        {
            CountNumbers = CountOperator + 1;
        }

        /// <summary>
        /// Закрытие скобок
        /// </summary>
        /// <returns></returns>
        public bool CheckParentheses(string input)
        {
            var str = new StringBuilder(input);
            int TrueLeft = 0;
            int TrueRight = 0;
        
            for(int i = 0; i < str.Length; i++) {
              if(str[i] == ')') { 
                return false;
              }
          
              if(str[i] == '(') {
                TrueLeft++;
                str[i] = ' ';
            
                for(int j = i; j < str.Length; j++) {
                  if(str[j] == ')') {
                    str[j] = ' ';
                    TrueRight++;
                    break;
                  }
                }
              }
            }
            if(TrueLeft > TrueRight) {
              return false;
            }
            // Your code here
            return true;
        }

        /// <summary>
        /// Проверка строки
        /// </summary>
        /// <param name="input">строка</param>
        /// <returns></returns>
        public bool PreliminaryParse(string input)
        {
            Input = new StringBuilder(input);
            Input.Replace(" ", "");// Убираем пробелы
            int lefthParentheses = -1;
            // Добавляем знак умножение между числом и скобкой
            for (int i = 1; i < Input.Length - 1; i++)
            {
                if (char.IsDigit(Input[i]) && i < Input.Length - 1 && Input[i + 1] == '(')
                {
                    Input.Insert(i + 1, "*");
                }
                if (Input[i] == ')' && i < Input.Length - 1 && char.IsDigit(Input[i + 1]))
                {
                    Input.Insert(i + 1, "*");
                }
                if (Input[i] == '-' && i < Input.Length - 1 && Input[i + 1] == '(' && Input[i + 2] == '-')
                {
                    for (int itr = i; itr < Input.Length; itr++)
                    {
                        if (Input[itr] == ')' && lefthParentheses == 0)
                        {
                            Input.Insert(itr, ")");
                            Input.Remove(i, 1);
                            Input.Insert(i, "(-1*");
                            lefthParentheses = -1;
                            break;
                        }
                        if (Input[itr] == '(')
                        {
                            lefthParentheses++;
                        }
                        if (Input[itr] == ')')
                        {
                            lefthParentheses--;
                        }
                    }
                }
            }
            if (CheckParentheses(input))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Парсер строки
        /// </summary>
        /// <param name="input"></param>
        public void Parse(string input)
        {
            Input = new StringBuilder(input);
            FindCountOperator();
            FindCountNumbers();
            char elementCut = '|';
            numbers = new string[CountOperator + 1];
            operation = new string[CountOperator];
            int idArrayOp = 0;
            // знаки не начинают строку, только числа
            for (int i = 1; i < Input.Length - 1; i++)
            {
                // Добавляет знаки (+-*/) знаки не могут быть в начале строки
                if (Input[i] == '+' || Input[i] == '-' ||
                    Input[i] == '*' || Input[i] == '/' ||
                    Input[i] == '^')
                {
                    operation[idArrayOp] = Input[i].ToString();
                    Input[i] = elementCut;
                    idArrayOp++;
                    if (Input[i + 1] == '-')
                    {
                        i++;
                    }
                }
            }
            numbers = Input.ToString().Split(new char[] { elementCut }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
