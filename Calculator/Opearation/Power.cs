﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Opearation
{
    internal sealed class Power : Computation
    {
        public Power(double value1, double value2) : base(value1, value2)
        {
        }

        public override void Function_()
        {
            try
            {
                if (Value2 == 0)
                {
                    Result = "1";
                }
                else
                {
                    double tmpMultiplication = Value1;
                    for (int i = 1; i < Value2; i++)
                    {
                        Value1 *= tmpMultiplication;
                    }
                    Result = checked(Value1).ToString();
                }
            }
            catch (System.OverflowException e)
            {
                // The following line displays information about the error.
                Result = "Overflow";
            }
        }
    }
}
