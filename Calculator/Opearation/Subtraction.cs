﻿namespace Calculator.Opearation
{
    internal sealed class Subtraction : Computation
    {
        public Subtraction(double value1, double value2) : base(value1, value2)
        {
        }

        public override void Function_()
        {
            try
            {
                // The following line raises an exception because it is checked.
                Result = checked(Value1 - Value2).ToString();
            }
            catch (System.OverflowException e)
            {
                // The following line displays information about the error.
                Result = "Overflow";
            }
        }
    }
}
