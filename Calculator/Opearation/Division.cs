﻿namespace Calculator.Opearation
{
    internal sealed class Division : Computation
    {
        public Division(double value1, double value2) : base(value1, value2)
        {
        }

        public override void Function_()
        {
            if (Value2 == 0)
            {
                Result = "infinite";
            }
            else
            {
                try
                {
                    Result = checked(Value1 / Value2).ToString();
                }
                catch (System.OverflowException e)
                {
                    // The following line displays information about the error.
                    Result = "Overflow";
                }
            }
        }
    }
}
