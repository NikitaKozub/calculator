﻿using System;

namespace Calculator.Opearation
{
    /// <summary>
    ///  Класс операция на числами
    /// </summary>
    internal abstract class Computation: IOperation
    {
        public double Value1 { get; set; }
        public double Value2 { get; set; }
        public string Result { get; set; }

        public Computation(double value1, double value2)
        {
            Value1 = value1;
            Value2 = value2;
        }

        public abstract void Function_();
    }
}
