﻿using Calculator.Opearation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
    public class Calculator_
    {
        public string Output { get; set; }// результат вычислений
        History History { get; set; }
        public string Input;
        private ExpressionToCalculate expression;
        private string[] znaki1 = new string[] { "*", "/" , "^"};
        private string[] znaki2 = new string[] { "+", "-" };
        private int[] poradok;
        private Dictionary<string, Computation> operation = new Dictionary<string, Computation>(5) { { "/", new Division(0, 0) }, 
                                                                                                     {"*", new Multiplication(0, 0) }, 
                                                                                                     {"+", new Plus(0, 0) }, 
                                                                                                     {"-", new Subtraction(0, 0) },
                                                                                                     {"^", new Power(0, 0)}};

        public Calculator_()
        {
            History = new History();
            Output = "";
            expression = new ExpressionToCalculate();
        }

        private bool IsParentheses()
        {
            for (int i = 0; i < expression.Input.Length; i++)
            {
                if (expression.Input[i] == '(')
                {
                    return true;
                }
            }
            return false;
        }
        
        public void Calculation()
        {
            bool well = expression.PreliminaryParse(Input);
            if (well)
            {
                int parenthesisStart = -1;
                int parenthesisEnd = -1;
                while (true)
                {
                    for (int i = 0; i < expression.Input.Length; i++)
                    {
                        if (expression.Input[i] == '(')
                        {
                            parenthesisStart = i;
                            for (int j = i + 1; j < expression.Input.Length; j++)
                            {
                                if (expression.Input[j] == '(')
                                {
                                    break;
                                }
                                if (expression.Input[j] == ')')
                                {
                                    if (i+1 == j)
                                    {
                                        Output = "Ошибка в поле ввода";
                                        History.Add(Input, Output);
                                        return;
                                    }
                                    parenthesisEnd = j;
                                    string tmp = CalculationOfNumbers(expression.Input.ToString(parenthesisStart + 1, parenthesisEnd - parenthesisStart - 1) );
                                    expression.Input = expression.Input.Remove(parenthesisStart, parenthesisEnd - parenthesisStart + 1);
                                    expression.Input = expression.Input.Insert(parenthesisStart, tmp);
                                    break;
                                }
                            }
                        }
                        if(!IsParentheses()) // нет скобок
                        {
                            Output = CalculationOfNumbers(expression.Input.ToString());
                            History.Add(Input, Output);
                            return;
                        }
                    }
                }
            }
            else
            {
                Output = "Ошибка в поле ввода";
                History.Add(Input, Output);
                return;
            }
        }


        public int[] DelElementArray(int[] array, int id)
        {
            return array = array.Where((val, idx) => idx != id).ToArray();
        }

        /// <summary>
        /// Удалить из массива строк по индексу
        /// </summary>
        /// <param name="array"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string[] DelElementArray(string[] array, int id)
        {
            return array = array.Where((val, idx) => idx != id).ToArray();
        }

        /// <summary>
        /// Вычисление порядка операций
        /// </summary>
        /// <param name="op">массив с операциями</param>
        /// <returns>операции в правильном порядке</returns>
        public int[] OrderOfOperations(string[] op)
        {
            int[] order = new int[op.Length];
            int idOrder = 0;
            for (int i = 0; i < op.Length; i++)
            {
                for (int j = 0; j < znaki1.Length; j++)
                {
                    if (op[i] == znaki1[j])
                    {
                        order[idOrder] = Convert.ToInt32(i);
                        idOrder++;
                    }
                }
            }
            for (int i = 0; i < op.Length; i++)
            {
                for (int j = 0; j < znaki2.Length; j++)
                {
                    if (op[i] == znaki2[j])
                    {
                        order[idOrder] = Convert.ToInt32(i);
                        idOrder++;
                    }
                }
            }

            return order;
        }

        public int[] UpdateOrderOfOperations(string[] order)
        {
            return OrderOfOperations(order);
        }

        /// <summary>
        /// действия внутри скобок
        /// или просто действия
        /// </summary>
        /// <param name="parenthesisStart"></param>
        /// <param name="parenthesisEnd"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public string CalculationOfNumbers(string str)
        {
            ExpressionToCalculate expression = new ExpressionToCalculate();
            expression.Parse(str);
            poradok = OrderOfOperations(expression.operation);
            while (expression.operation.Length > 0)
            {
                for (int i = 0; i < poradok.Length;)
                {
                    //чтоб вспомнить, как это раотает смотри строку ниже
                    //private Dictionary<string, Computation> operation = new Dictionary<string, Computation>(4) { { "/", new Division(0, 0) }, {"*", new Multiplication(0, 0) }, {"+", new Plus(0, 0) }, {"-", new Subtraction(0, 0) }};
                    Computation operat = operation[expression.operation[poradok[i]]];
                    operat.Value1 = Convert.ToDouble(expression.numbers[poradok[i]]);
                    operat.Value2 = Convert.ToDouble(expression.numbers[poradok[i] + 1]);
                    operat.Function_();
                    if (operat.Result == "infinite")
                    {
                        return operat.Result;
                    }
                    expression.numbers[poradok[i]] = operat.Result;
                    expression.numbers = DelElementArray(expression.numbers, poradok[i] + 1);
                    expression.operation = DelElementArray(expression.operation, poradok[i]);
                    poradok = DelElementArray(poradok, poradok[i]);
                    poradok = UpdateOrderOfOperations(expression.operation);
                    break;
                }
            }
            return expression.numbers[0].ToString();
        }
    }
}
