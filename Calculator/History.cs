﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class History
    {
        private string Path;
        public string dateTime { get; set; }// дата вычисления и время
        public string calculation { get; set; }// вычисление
        public ObservableCollection<History> Record { get; set; }

        public History()
        {
            Path = "./History.txt";
            CheckHistory();
            Record = new ObservableCollection<History>();
        }

        private void CheckHistory()
        {
            if (!File.Exists(Path))
            {
                File.Create(Path).Close();
            }
        }

        public void Add(string input, string result)
        {
            using (StreamWriter sw = new StreamWriter(Path, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(DateTime.Now + "|" + input + "=" + result);
            }
        }

        /// <summary>
        /// Прочитать всю историю вычислений
        /// </summary>
        /// <returns></returns>
        public void ReadHistory()
        {
            string[] tmp = File.ReadAllLines(Path);
            Record.Clear();
            for (int i = 0; i < tmp.Length; i++)
            {
                History history = new History();
                history.dateTime = tmp[i].Substring(0, 19).Replace("|", "");
                history.calculation = tmp[i].Substring(19, tmp[i].Length - 19).Replace("|", "");
                Record.Add(history);
            }
        }

        /// <summary>
        /// Удалить по значению
        /// </summary>
        /// <param name="element">объект удаления из списка</param>
        public void Remove(History element)
        {
            Record.Remove(element);

            using (StreamWriter sw = new StreamWriter(Path, false, System.Text.Encoding.Default))
            {
                foreach (History itr in Record)
                {
                    sw.WriteLine(itr.dateTime + "|" + itr.calculation);
                }
            }
        }
    }
}
