﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Calculator.Model
{
    public class CalculatorModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Поле ввода
        /// </summary>
        private StringBuilder _Input = new StringBuilder();
        public string Input
        {
            get { return _Input.ToString(); }
            set
            {
                if (value == "CE")
                {
                    _Input.Clear();
                    OnPropertyChanged("Input");
                    return;
                }
                if (value == "C")
                {
                    if (_Input.Length >= 1)
                    {
                        if (_Input.Length == 2 && _Input[0] == '-')
                        {
                            _Input.Clear();
                            OnPropertyChanged("Input");
                            return;
                        }
                        _Input.Remove(_Input.Length - 1, 1);
                        OnPropertyChanged("Input");
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                _Input.Replace("infinite", "");
                _Input.Replace("Ошибка в поле ввода", "");
                _Input.Append(value);
                OnPropertyChanged("Input");
            }
        }

        /// <summary>
        /// Нажата ли цифра
        /// </summary>
        private bool _numberEnabled;
        public bool NumberEnabled
        {
            get { return _numberEnabled; }
            set
            {
                _numberEnabled = value;
                OnPropertyChanged("NumberEnabled");
            }
        }

        /// <summary>
        /// Нажат ли знак
        /// </summary>
        private bool _operationEnabled;
        public bool OperationEnabled
        {
            get { return _operationEnabled; }
            set
            {
                _operationEnabled = value;
                OnPropertyChanged("OperationEnabled");
            }
        }

        /// <summary>
        /// Нажата ли запятая
        /// </summary>
        private bool _pointEnabled;
        public bool PointEnabled
        {
            get { return _pointEnabled; }
            set
            {
                _pointEnabled = value;
                OnPropertyChanged("PointEnabled");
            }
        }

        /// <summary>
        /// Нажато ли равно
        /// </summary>
        private bool _сalculateEnabled;
        public bool CalculateEnabled
        {
            get { return _сalculateEnabled; }
            set
            {
                _сalculateEnabled = value;
                OnPropertyChanged("CalculateEnabled");
            }
        }
        
        /// <summary>
        /// Нажата ли левая кнопка
        /// </summary>
        private bool _parenthesesLefthEnabled;
        public bool ParenthesesLefthEnabled
        {
            get { return _parenthesesLefthEnabled; }
            set
            {
                _parenthesesLefthEnabled = value;
                OnPropertyChanged("ParenthesesLefthEnabled");
            }
        }

        /// <summary>
        /// Нажата ли правая кнопка
        /// </summary>
        private bool _parenthesesRightEnabled;
        public bool ParenthesesRightEnabled
        {
            get { return _parenthesesRightEnabled; }
            set
            {
                _parenthesesRightEnabled = value;
                OnPropertyChanged("ParenthesesRightEnabled");
            }
        }

        /// <summary>
        /// История вычислений
        /// </summary>
        private IEnumerable<History> _historyList;
        public IEnumerable<History> HistoryList
        {
            get
            {
                return _historyList;
            }
            set
            {
                _historyList = value;
                OnPropertyChanged("HistoryList");
            }
        }


        private string selectedHistory;
        public string SelectedHistory
        {
            get { return selectedHistory; }
            set
            {
                selectedHistory = value;
                OnPropertyChanged("SelectedHistory");
            }
        }
    }
}
