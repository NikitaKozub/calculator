﻿using Calculator;
using Calculator.Model;
using System;
using System.Collections.Generic;

namespace ViewModel
{
    public class CalculatorViewModel
    {
        public CalculatorModel CalculatorModel { get; set; }
        public Calculator_ calculator;
        public History historyFail;

        public CalculatorViewModel()
        {
            historyFail = new History();
            historyFail.ReadHistory();
            CalculatorModel = new CalculatorModel()
            {
                OperationEnabled = false,
                NumberEnabled = true,
                PointEnabled = false,
                CalculateEnabled = false,
                ParenthesesRightEnabled = false,
                ParenthesesLefthEnabled = true,
                HistoryList = historyFail.Record
            };
            calculator = new Calculator_();
        }

        /// <summary>
        /// Ввод чисел 0-9
        /// </summary>
        RelayCommand _buttonNumber;
        public RelayCommand ButtonNumber
        {
            get
            {
                return _buttonNumber ??
                    (_buttonNumber = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonNumber != null)
                        {
                            CalculatorModel.Input = selectedItem.ToString();
                            CalculatorModel.OperationEnabled = true;
                            CalculatorModel.PointEnabled = true;
                            CalculatorModel.CalculateEnabled = true;
                        }
                    }));
            }
        }

        /// <summary>
        /// Ввод операций +-*/
        /// </summary>
        RelayCommand _buttonOperation;
        public RelayCommand ButtonOperation
        {
            get
            {
                return _buttonOperation ??
                    (_buttonOperation = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonOperation != null)
                        {
                            CalculatorModel.Input = selectedItem.ToString();
                            CalculatorModel.OperationEnabled = false;
                            CalculatorModel.PointEnabled = false;
                            CalculatorModel.CalculateEnabled = false;
                        }
                    }));
            }
        }

        /// <summary>
        /// Удаление одного символа, всей строки
        /// </summary>
        RelayCommand _buttonDelete;
        public RelayCommand ButtonDelete
        {
            get
            {
                return _buttonDelete ??
                    (_buttonDelete = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonDelete != null)
                        {
                            CalculatorModel.Input = selectedItem.ToString();
                        }
                    }));
            }
        }

        /// <summary>
        /// Удалить из списка вычислений один элемент
        /// </summary>
        RelayCommand _listDelete;
        public RelayCommand ListDelete
        {
            get
            {
                return _listDelete ??
                    (_listDelete = new RelayCommand((SelectedItem) =>
                    {
                        History history = SelectedItem as History;
                        if (history != null)
                        {
                            historyFail.Remove(history);
                        }
                    }));
            }
        }

        /// <summary>
        /// Вычисление
        /// </summary>
        RelayCommand _buttonCalculate;
        public RelayCommand ButtonCalculate
        {
            get
            {
                return _buttonCalculate ??
                    (_buttonCalculate = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonCalculate != null)
                        {
                            calculator.Input = CalculatorModel.Input;
                            CalculatorModel.Input = "CE";
                            calculator.Calculation();

                            CalculatorModel.Input = calculator.Output;
                            historyFail.ReadHistory();
                            CalculatorModel.HistoryList = historyFail.Record;
                        }
                    }));
            }
        }

        /// <summary>
        /// Добавить скобку
        /// </summary>
        RelayCommand _buttonParentheses;
        public RelayCommand ButtonParentheses
        {
            get
            {
                return _buttonParentheses ??
                    (_buttonParentheses = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonParentheses != null)
                        {
                            CalculatorModel.Input = selectedItem.ToString();
                            CalculatorModel.PointEnabled = false;
                            CalculatorModel.CalculateEnabled = true;
                            CalculatorModel.ParenthesesRightEnabled = true;
                        }
                    }));
            }
        }

        /// <summary>
        /// Добавить запятую
        /// </summary>
        RelayCommand _buttonPoint;
        public RelayCommand ButtonPoint
        {
            get
            {
                return _buttonPoint ??
                    (_buttonPoint = new RelayCommand((selectedItem) =>
                    {
                        if (_buttonPoint != null)
                        {
                            CalculatorModel.Input = selectedItem.ToString();
                            CalculatorModel.PointEnabled = false;
                            CalculatorModel.CalculateEnabled = false;
                        }
                    }));
            }
        }
    }
}
