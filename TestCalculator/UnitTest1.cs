﻿using System;
using Calculator;
using Calculator.Opearation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCalculator
{
    [TestClass]
    public class UnitTest1
    {
        Calculator_ calc;
        ExpressionToCalculate expressionToCalculate;

        public UnitTest1()
        {
            calc = new Calculator_();
        }

        [TestMethod]
        public void TestMethodCalculation()
        {
            Assert.AreEqual("7,6", calc.CalculationOfNumbers("3,1+4,5"));
            Assert.AreEqual("-1,1", calc.CalculationOfNumbers("3,5-4,6"));
            Assert.AreEqual("0,3", calc.CalculationOfNumbers("0,1+0,2"));

            Assert.AreEqual("7", calc.CalculationOfNumbers("3+4"));
            Assert.AreEqual("1", calc.CalculationOfNumbers("-3+4"));
            Assert.AreEqual("14", calc.CalculationOfNumbers("10+4"));
            Assert.AreEqual("-6", calc.CalculationOfNumbers("-10+4"));
            Assert.AreEqual("41", calc.CalculationOfNumbers("1+40"));

            Assert.AreEqual("17", calc.CalculationOfNumbers("3+4+10"));
            Assert.AreEqual("40", calc.CalculationOfNumbers("3+40-3"));
            Assert.AreEqual("31", calc.CalculationOfNumbers("30+4-3"));
            Assert.AreEqual("9", calc.CalculationOfNumbers("3*4-3"));
            Assert.AreEqual("23", calc.CalculationOfNumbers("3+4+8*2"));

            Assert.AreEqual("-5", calc.CalculationOfNumbers("4/4-3*2"));
            Assert.AreEqual("16", calc.CalculationOfNumbers("3*4+8/2"));
            Assert.AreEqual("15", calc.CalculationOfNumbers("1*3+4*3"));
            Assert.AreEqual("8", calc.CalculationOfNumbers("3*4-3-1"));
            Assert.AreEqual("15", calc.CalculationOfNumbers("3+4+8/2*2"));

            Assert.AreEqual("9", calc.CalculationOfNumbers("3^2"));
            Assert.AreEqual("3", calc.CalculationOfNumbers("3^1"));
            Assert.AreEqual("27", calc.CalculationOfNumbers("3 ^ 3"));
            Assert.AreEqual("32", calc.CalculationOfNumbers("2 ^ 5"));
            Assert.AreEqual("1", calc.CalculationOfNumbers("5 ^ 0"));





            Assert.AreEqual("infinite", calc.CalculationOfNumbers("1/0"));
        }

        [TestMethod]
        public void TestMethodParse()
        {
            expressionToCalculate = new ExpressionToCalculate();
            expressionToCalculate.Parse("-5*-3+-7+1-1");
            Assert.AreEqual("-5", expressionToCalculate.numbers[0]);
            Assert.AreEqual("-3", expressionToCalculate.numbers[1]);
            Assert.AreEqual("-7", expressionToCalculate.numbers[2]);
            Assert.AreEqual("1", expressionToCalculate.numbers[3]);
            Assert.AreEqual("1", expressionToCalculate.numbers[4]);

            Assert.AreEqual("*", expressionToCalculate.operation[0]);
            Assert.AreEqual("+", expressionToCalculate.operation[1]);
            Assert.AreEqual("+", expressionToCalculate.operation[2]);
            Assert.AreEqual("-", expressionToCalculate.operation[3]);
            Assert.AreEqual(4, expressionToCalculate.operation.Length);
        }

        [TestMethod]
        public void TestMethodParentheses()
        {
            calc.Input = "(-1(3+4) + (-3*3))/2";
            calc.Calculation();
            Assert.AreEqual("-8", calc.Output);

            calc.Input = "(3+4) + (3*3)";
            calc.Calculation();
            Assert.AreEqual("16", calc.Output);

            calc.Input = "(3*4) + (3*3)";
            calc.Calculation();
            Assert.AreEqual("21", calc.Output);

            calc.Input = "(3^1)*2";
            calc.Calculation();
            Assert.AreEqual("6", calc.Output);
            
            calc.Input = "(12/4) + 2(3+3)";
            calc.Calculation();
            Assert.AreEqual("15", calc.Output);

            calc.Input = "(12/4) + (3+3)*2";
            calc.Calculation();
            Assert.AreEqual("15", calc.Output);

            calc.Input = "((3+4) + (3*3))/2";
            calc.Calculation();
            Assert.AreEqual("8", calc.Output);

            calc.Input = "((3+4)))";
            calc.Calculation();
            Assert.AreEqual("Ошибка в поле ввода", calc.Output);
                        
            calc.Input = "12 * 123 / -(-5 + 2)";
            calc.Calculation();
            Assert.AreEqual("492", calc.Output);

            calc.Input = "(123,45*(678,90 / (-2,5+ 11,5)-(((80 -(19))) *33,25)) / 20) - (-123,45*(678,90 / (-2,5+ 11,5)-(((80 -(19))) *33,25)) / 20) + (13 - 2)/ -(-11) ";
            calc.Calculation();
            Assert.AreEqual("-24106,52175", calc.Output);
        }
    }
}
